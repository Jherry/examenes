﻿namespace ejercicio_2
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpDatosgenerales = new System.Windows.Forms.GroupBox();
            this.rbFemenino = new System.Windows.Forms.RadioButton();
            this.rbMasculino = new System.Windows.Forms.RadioButton();
            this.label4 = new System.Windows.Forms.Label();
            this.txtDNI = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.dtFecnac = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.txtAN = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.grpSueldo = new System.Windows.Forms.GroupBox();
            this.lblTotal = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtUtilidades = new System.Windows.Forms.TextBox();
            this.txtEscolaridad = new System.Windows.Forms.TextBox();
            this.linkLabel2 = new System.Windows.Forms.LinkLabel();
            this.txtVida = new System.Windows.Forms.TextBox();
            this.txtESSALUD = new System.Windows.Forms.TextBox();
            this.txtAFP = new System.Windows.Forms.TextBox();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.label12 = new System.Windows.Forms.Label();
            this.txtMonto = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.grpDatos = new System.Windows.Forms.GroupBox();
            this.txtSueldo = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.btnCalcular = new System.Windows.Forms.Button();
            this.lstContratar = new System.Windows.Forms.ListBox();
            this.label9 = new System.Windows.Forms.Label();
            this.btnQuitar = new System.Windows.Forms.Button();
            this.btnAgregar = new System.Windows.Forms.Button();
            this.lstPuestos = new System.Windows.Forms.ListBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cboSede = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.grpExperiencia = new System.Windows.Forms.GroupBox();
            this.btnEvaluar = new System.Windows.Forms.Button();
            this.txtActividad = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtSalarial = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.rbMasaños = new System.Windows.Forms.RadioButton();
            this.rb4años = new System.Windows.Forms.RadioButton();
            this.rb2años = new System.Windows.Forms.RadioButton();
            this.rbExperiencia = new System.Windows.Forms.RadioButton();
            this.grpHabilidades = new System.Windows.Forms.GroupBox();
            this.txtOtros = new System.Windows.Forms.TextBox();
            this.chkOtros = new System.Windows.Forms.CheckBox();
            this.chkCeremonia = new System.Windows.Forms.CheckBox();
            this.chkMalabarista = new System.Windows.Forms.CheckBox();
            this.chkMagia = new System.Windows.Forms.CheckBox();
            this.chkImitar = new System.Windows.Forms.CheckBox();
            this.chkBailar = new System.Windows.Forms.CheckBox();
            this.chkPayaso = new System.Windows.Forms.CheckBox();
            this.chkCantar = new System.Windows.Forms.CheckBox();
            this.grpDatosgenerales.SuspendLayout();
            this.grpSueldo.SuspendLayout();
            this.grpDatos.SuspendLayout();
            this.grpExperiencia.SuspendLayout();
            this.grpHabilidades.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpDatosgenerales
            // 
            this.grpDatosgenerales.Controls.Add(this.rbFemenino);
            this.grpDatosgenerales.Controls.Add(this.rbMasculino);
            this.grpDatosgenerales.Controls.Add(this.label4);
            this.grpDatosgenerales.Controls.Add(this.txtDNI);
            this.grpDatosgenerales.Controls.Add(this.label3);
            this.grpDatosgenerales.Controls.Add(this.dtFecnac);
            this.grpDatosgenerales.Controls.Add(this.label2);
            this.grpDatosgenerales.Controls.Add(this.txtAN);
            this.grpDatosgenerales.Controls.Add(this.label1);
            this.grpDatosgenerales.Location = new System.Drawing.Point(12, 21);
            this.grpDatosgenerales.Name = "grpDatosgenerales";
            this.grpDatosgenerales.Size = new System.Drawing.Size(502, 143);
            this.grpDatosgenerales.TabIndex = 10;
            this.grpDatosgenerales.TabStop = false;
            this.grpDatosgenerales.Text = "Datos Generales:";
            // 
            // rbFemenino
            // 
            this.rbFemenino.AutoSize = true;
            this.rbFemenino.Location = new System.Drawing.Point(314, 110);
            this.rbFemenino.Name = "rbFemenino";
            this.rbFemenino.Size = new System.Drawing.Size(71, 17);
            this.rbFemenino.TabIndex = 8;
            this.rbFemenino.TabStop = true;
            this.rbFemenino.Text = "Femenino";
            this.rbFemenino.UseVisualStyleBackColor = true;
            // 
            // rbMasculino
            // 
            this.rbMasculino.AutoSize = true;
            this.rbMasculino.Location = new System.Drawing.Point(143, 110);
            this.rbMasculino.Name = "rbMasculino";
            this.rbMasculino.Size = new System.Drawing.Size(73, 17);
            this.rbMasculino.TabIndex = 7;
            this.rbMasculino.TabStop = true;
            this.rbMasculino.Text = "Masculino";
            this.rbMasculino.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 112);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Genero:";
            // 
            // txtDNI
            // 
            this.txtDNI.Location = new System.Drawing.Point(127, 82);
            this.txtDNI.Name = "txtDNI";
            this.txtDNI.Size = new System.Drawing.Size(154, 20);
            this.txtDNI.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 85);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "DNI:";
            // 
            // dtFecnac
            // 
            this.dtFecnac.Location = new System.Drawing.Point(128, 52);
            this.dtFecnac.Name = "dtFecnac";
            this.dtFecnac.Size = new System.Drawing.Size(306, 20);
            this.dtFecnac.TabIndex = 3;
            this.dtFecnac.ValueChanged += new System.EventHandler(this.dtFecnac_ValueChanged_1);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(111, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Fecha de Nacimiento:";
            // 
            // txtAN
            // 
            this.txtAN.Location = new System.Drawing.Point(128, 23);
            this.txtAN.Name = "txtAN";
            this.txtAN.Size = new System.Drawing.Size(306, 20);
            this.txtAN.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(105, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nombres y Apellidos:";
            // 
            // grpSueldo
            // 
            this.grpSueldo.Controls.Add(this.lblTotal);
            this.grpSueldo.Controls.Add(this.label16);
            this.grpSueldo.Controls.Add(this.label17);
            this.grpSueldo.Controls.Add(this.label15);
            this.grpSueldo.Controls.Add(this.label14);
            this.grpSueldo.Controls.Add(this.label13);
            this.grpSueldo.Controls.Add(this.txtUtilidades);
            this.grpSueldo.Controls.Add(this.txtEscolaridad);
            this.grpSueldo.Controls.Add(this.linkLabel2);
            this.grpSueldo.Controls.Add(this.txtVida);
            this.grpSueldo.Controls.Add(this.txtESSALUD);
            this.grpSueldo.Controls.Add(this.txtAFP);
            this.grpSueldo.Controls.Add(this.linkLabel1);
            this.grpSueldo.Controls.Add(this.label12);
            this.grpSueldo.Controls.Add(this.txtMonto);
            this.grpSueldo.Controls.Add(this.label11);
            this.grpSueldo.Location = new System.Drawing.Point(528, 342);
            this.grpSueldo.Name = "grpSueldo";
            this.grpSueldo.Size = new System.Drawing.Size(502, 209);
            this.grpSueldo.TabIndex = 46;
            this.grpSueldo.TabStop = false;
            this.grpSueldo.Text = "Sueldo Aproximado";
            // 
            // lblTotal
            // 
            this.lblTotal.AutoSize = true;
            this.lblTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotal.Location = new System.Drawing.Point(218, 169);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(161, 25);
            this.lblTotal.TabIndex = 52;
            this.lblTotal.Text = "TOTAL NETO:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(294, 105);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(85, 13);
            this.label16.TabIndex = 51;
            this.label16.Text = "Escolaridad (8%)";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(294, 128);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(82, 13);
            this.label17.TabIndex = 50;
            this.label17.Text = "Utilidades (15%)";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(17, 124);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(86, 13);
            this.label15.TabIndex = 49;
            this.label15.Text = "ESSALUD (10%)";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(17, 147);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(71, 13);
            this.label14.TabIndex = 48;
            this.label14.Text = "Vida Ley (5%)";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(17, 98);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(56, 13);
            this.label13.TabIndex = 47;
            this.label13.Text = "AFP (12%)";
            // 
            // txtUtilidades
            // 
            this.txtUtilidades.Location = new System.Drawing.Point(403, 128);
            this.txtUtilidades.Name = "txtUtilidades";
            this.txtUtilidades.Size = new System.Drawing.Size(74, 20);
            this.txtUtilidades.TabIndex = 46;
            // 
            // txtEscolaridad
            // 
            this.txtEscolaridad.Location = new System.Drawing.Point(403, 102);
            this.txtEscolaridad.Name = "txtEscolaridad";
            this.txtEscolaridad.Size = new System.Drawing.Size(74, 20);
            this.txtEscolaridad.TabIndex = 45;
            // 
            // linkLabel2
            // 
            this.linkLabel2.AutoSize = true;
            this.linkLabel2.LinkColor = System.Drawing.Color.Black;
            this.linkLabel2.Location = new System.Drawing.Point(338, 66);
            this.linkLabel2.Name = "linkLabel2";
            this.linkLabel2.Size = new System.Drawing.Size(56, 13);
            this.linkLabel2.TabIndex = 44;
            this.linkLabel2.TabStop = true;
            this.linkLabel2.Text = "Beneficios";
            // 
            // txtVida
            // 
            this.txtVida.Location = new System.Drawing.Point(109, 144);
            this.txtVida.Name = "txtVida";
            this.txtVida.Size = new System.Drawing.Size(74, 20);
            this.txtVida.TabIndex = 43;
            // 
            // txtESSALUD
            // 
            this.txtESSALUD.Location = new System.Drawing.Point(109, 121);
            this.txtESSALUD.Name = "txtESSALUD";
            this.txtESSALUD.Size = new System.Drawing.Size(74, 20);
            this.txtESSALUD.TabIndex = 42;
            // 
            // txtAFP
            // 
            this.txtAFP.Location = new System.Drawing.Point(109, 95);
            this.txtAFP.Name = "txtAFP";
            this.txtAFP.Size = new System.Drawing.Size(74, 20);
            this.txtAFP.TabIndex = 41;
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.LinkColor = System.Drawing.Color.Black;
            this.linkLabel1.Location = new System.Drawing.Point(9, 66);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(64, 13);
            this.linkLabel1.TabIndex = 3;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Descuentos";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(178, 32);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(63, 12);
            this.label12.TabIndex = 2;
            this.label12.Text = "Nuevos Soles";
            // 
            // txtMonto
            // 
            this.txtMonto.Location = new System.Drawing.Point(79, 26);
            this.txtMonto.Name = "txtMonto";
            this.txtMonto.Size = new System.Drawing.Size(93, 20);
            this.txtMonto.TabIndex = 1;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(36, 29);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(37, 13);
            this.label11.TabIndex = 0;
            this.label11.Text = "Monto";
            // 
            // grpDatos
            // 
            this.grpDatos.Controls.Add(this.txtSueldo);
            this.grpDatos.Controls.Add(this.label10);
            this.grpDatos.Controls.Add(this.btnCalcular);
            this.grpDatos.Controls.Add(this.lstContratar);
            this.grpDatos.Controls.Add(this.label9);
            this.grpDatos.Controls.Add(this.btnQuitar);
            this.grpDatos.Controls.Add(this.btnAgregar);
            this.grpDatos.Controls.Add(this.lstPuestos);
            this.grpDatos.Controls.Add(this.label8);
            this.grpDatos.Controls.Add(this.cboSede);
            this.grpDatos.Controls.Add(this.label7);
            this.grpDatos.Location = new System.Drawing.Point(528, 20);
            this.grpDatos.Name = "grpDatos";
            this.grpDatos.Size = new System.Drawing.Size(502, 310);
            this.grpDatos.TabIndex = 45;
            this.grpDatos.TabStop = false;
            this.grpDatos.Text = "Datos Generales";
            // 
            // txtSueldo
            // 
            this.txtSueldo.Location = new System.Drawing.Point(98, 268);
            this.txtSueldo.Name = "txtSueldo";
            this.txtSueldo.Size = new System.Drawing.Size(74, 20);
            this.txtSueldo.TabIndex = 40;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(30, 271);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(43, 13);
            this.label10.TabIndex = 39;
            this.label10.Text = "Sueldo:";
            // 
            // btnCalcular
            // 
            this.btnCalcular.Location = new System.Drawing.Point(270, 264);
            this.btnCalcular.Name = "btnCalcular";
            this.btnCalcular.Size = new System.Drawing.Size(207, 26);
            this.btnCalcular.TabIndex = 38;
            this.btnCalcular.Text = "Calcular Monto a Pagar";
            this.btnCalcular.UseVisualStyleBackColor = true;
            this.btnCalcular.Click += new System.EventHandler(this.btnCalcular_Click_1);
            // 
            // lstContratar
            // 
            this.lstContratar.FormattingEnabled = true;
            this.lstContratar.Location = new System.Drawing.Point(270, 129);
            this.lstContratar.Name = "lstContratar";
            this.lstContratar.Size = new System.Drawing.Size(211, 121);
            this.lstContratar.TabIndex = 37;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(294, 106);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(100, 13);
            this.label9.TabIndex = 36;
            this.label9.Text = "Puestos a Contratar";
            // 
            // btnQuitar
            // 
            this.btnQuitar.Location = new System.Drawing.Point(178, 209);
            this.btnQuitar.Name = "btnQuitar";
            this.btnQuitar.Size = new System.Drawing.Size(75, 23);
            this.btnQuitar.TabIndex = 35;
            this.btnQuitar.Text = "Quitar";
            this.btnQuitar.UseVisualStyleBackColor = true;
            this.btnQuitar.Click += new System.EventHandler(this.btnQuitar_Click_1);
            // 
            // btnAgregar
            // 
            this.btnAgregar.Location = new System.Drawing.Point(178, 180);
            this.btnAgregar.Name = "btnAgregar";
            this.btnAgregar.Size = new System.Drawing.Size(75, 23);
            this.btnAgregar.TabIndex = 34;
            this.btnAgregar.Text = "Agregar";
            this.btnAgregar.UseVisualStyleBackColor = true;
            this.btnAgregar.Click += new System.EventHandler(this.btnAgregar_Click_1);
            // 
            // lstPuestos
            // 
            this.lstPuestos.FormattingEnabled = true;
            this.lstPuestos.Location = new System.Drawing.Point(33, 129);
            this.lstPuestos.Name = "lstPuestos";
            this.lstPuestos.Size = new System.Drawing.Size(139, 121);
            this.lstPuestos.TabIndex = 33;
            this.lstPuestos.SelectedIndexChanged += new System.EventHandler(this.lstPuestos_SelectedIndexChanged_1);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(30, 106);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(102, 13);
            this.label8.TabIndex = 32;
            this.label8.Text = "Puestos Requeridos";
            // 
            // cboSede
            // 
            this.cboSede.FormattingEnabled = true;
            this.cboSede.Location = new System.Drawing.Point(132, 41);
            this.cboSede.Name = "cboSede";
            this.cboSede.Size = new System.Drawing.Size(246, 21);
            this.cboSede.TabIndex = 31;
            this.cboSede.SelectedIndexChanged += new System.EventHandler(this.cboSede_SelectedIndexChanged_1);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(30, 43);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(32, 13);
            this.label7.TabIndex = 30;
            this.label7.Text = "Sede";
            // 
            // grpExperiencia
            // 
            this.grpExperiencia.Controls.Add(this.btnEvaluar);
            this.grpExperiencia.Controls.Add(this.txtActividad);
            this.grpExperiencia.Controls.Add(this.label6);
            this.grpExperiencia.Controls.Add(this.txtSalarial);
            this.grpExperiencia.Controls.Add(this.label5);
            this.grpExperiencia.Controls.Add(this.rbMasaños);
            this.grpExperiencia.Controls.Add(this.rb4años);
            this.grpExperiencia.Controls.Add(this.rb2años);
            this.grpExperiencia.Controls.Add(this.rbExperiencia);
            this.grpExperiencia.Location = new System.Drawing.Point(11, 342);
            this.grpExperiencia.Name = "grpExperiencia";
            this.grpExperiencia.Size = new System.Drawing.Size(501, 209);
            this.grpExperiencia.TabIndex = 44;
            this.grpExperiencia.TabStop = false;
            this.grpExperiencia.Text = "Experiencia Laboral";
            // 
            // btnEvaluar
            // 
            this.btnEvaluar.Location = new System.Drawing.Point(193, 152);
            this.btnEvaluar.Name = "btnEvaluar";
            this.btnEvaluar.Size = new System.Drawing.Size(111, 33);
            this.btnEvaluar.TabIndex = 28;
            this.btnEvaluar.Text = "Evaluar Registro";
            this.btnEvaluar.UseVisualStyleBackColor = true;
            this.btnEvaluar.Click += new System.EventHandler(this.btnEvaluar_Click_1);
            // 
            // txtActividad
            // 
            this.txtActividad.Location = new System.Drawing.Point(193, 117);
            this.txtActividad.Name = "txtActividad";
            this.txtActividad.Size = new System.Drawing.Size(283, 20);
            this.txtActividad.TabIndex = 27;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(190, 91);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(132, 13);
            this.label6.TabIndex = 26;
            this.label6.Text = "Ultima actividad Realizada";
            // 
            // txtSalarial
            // 
            this.txtSalarial.Location = new System.Drawing.Point(193, 54);
            this.txtSalarial.Name = "txtSalarial";
            this.txtSalarial.Size = new System.Drawing.Size(283, 20);
            this.txtSalarial.TabIndex = 25;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(190, 32);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(137, 13);
            this.label5.TabIndex = 24;
            this.label5.Text = "Ingrese espectativa Salarial";
            // 
            // rbMasaños
            // 
            this.rbMasaños.AutoSize = true;
            this.rbMasaños.Location = new System.Drawing.Point(14, 113);
            this.rbMasaños.Name = "rbMasaños";
            this.rbMasaños.Size = new System.Drawing.Size(94, 17);
            this.rbMasaños.TabIndex = 23;
            this.rbMasaños.TabStop = true;
            this.rbMasaños.Text = "más de 4 años";
            this.rbMasaños.UseVisualStyleBackColor = true;
            this.rbMasaños.CheckedChanged += new System.EventHandler(this.rbMasaños_CheckedChanged_1);
            // 
            // rb4años
            // 
            this.rb4años.AutoSize = true;
            this.rb4años.Location = new System.Drawing.Point(14, 90);
            this.rb4años.Name = "rb4años";
            this.rb4años.Size = new System.Drawing.Size(66, 17);
            this.rb4años.TabIndex = 22;
            this.rb4años.TabStop = true;
            this.rb4años.Text = "2-4 años";
            this.rb4años.UseVisualStyleBackColor = true;
            this.rb4años.CheckedChanged += new System.EventHandler(this.rb4años_CheckedChanged_1);
            // 
            // rb2años
            // 
            this.rb2años.AutoSize = true;
            this.rb2años.Location = new System.Drawing.Point(14, 67);
            this.rb2años.Name = "rb2años";
            this.rb2años.Size = new System.Drawing.Size(66, 17);
            this.rb2años.TabIndex = 21;
            this.rb2años.TabStop = true;
            this.rb2años.Text = "0-2 años";
            this.rb2años.UseVisualStyleBackColor = true;
            this.rb2años.CheckedChanged += new System.EventHandler(this.rb2años_CheckedChanged_1);
            // 
            // rbExperiencia
            // 
            this.rbExperiencia.AutoSize = true;
            this.rbExperiencia.Location = new System.Drawing.Point(14, 44);
            this.rbExperiencia.Name = "rbExperiencia";
            this.rbExperiencia.Size = new System.Drawing.Size(97, 17);
            this.rbExperiencia.TabIndex = 20;
            this.rbExperiencia.TabStop = true;
            this.rbExperiencia.Text = "Sin experiencia";
            this.rbExperiencia.UseVisualStyleBackColor = true;
            this.rbExperiencia.CheckedChanged += new System.EventHandler(this.rbExperiencia_CheckedChanged_1);
            // 
            // grpHabilidades
            // 
            this.grpHabilidades.Controls.Add(this.txtOtros);
            this.grpHabilidades.Controls.Add(this.chkOtros);
            this.grpHabilidades.Controls.Add(this.chkCeremonia);
            this.grpHabilidades.Controls.Add(this.chkMalabarista);
            this.grpHabilidades.Controls.Add(this.chkMagia);
            this.grpHabilidades.Controls.Add(this.chkImitar);
            this.grpHabilidades.Controls.Add(this.chkBailar);
            this.grpHabilidades.Controls.Add(this.chkPayaso);
            this.grpHabilidades.Controls.Add(this.chkCantar);
            this.grpHabilidades.Location = new System.Drawing.Point(11, 171);
            this.grpHabilidades.Name = "grpHabilidades";
            this.grpHabilidades.Size = new System.Drawing.Size(501, 159);
            this.grpHabilidades.TabIndex = 43;
            this.grpHabilidades.TabStop = false;
            this.grpHabilidades.Text = "Habilidades";
            // 
            // txtOtros
            // 
            this.txtOtros.Location = new System.Drawing.Point(258, 123);
            this.txtOtros.Name = "txtOtros";
            this.txtOtros.Size = new System.Drawing.Size(218, 20);
            this.txtOtros.TabIndex = 18;
            // 
            // chkOtros
            // 
            this.chkOtros.AutoSize = true;
            this.chkOtros.Location = new System.Drawing.Point(214, 100);
            this.chkOtros.Name = "chkOtros";
            this.chkOtros.Size = new System.Drawing.Size(100, 17);
            this.chkOtros.TabIndex = 17;
            this.chkOtros.Text = "Otros (Explique)";
            this.chkOtros.UseVisualStyleBackColor = true;
            // 
            // chkCeremonia
            // 
            this.chkCeremonia.AutoSize = true;
            this.chkCeremonia.Location = new System.Drawing.Point(214, 77);
            this.chkCeremonia.Name = "chkCeremonia";
            this.chkCeremonia.Size = new System.Drawing.Size(132, 17);
            this.chkCeremonia.TabIndex = 16;
            this.chkCeremonia.Text = "Maestro de Ceremonia";
            this.chkCeremonia.UseVisualStyleBackColor = true;
            // 
            // chkMalabarista
            // 
            this.chkMalabarista.AutoSize = true;
            this.chkMalabarista.Location = new System.Drawing.Point(214, 54);
            this.chkMalabarista.Name = "chkMalabarista";
            this.chkMalabarista.Size = new System.Drawing.Size(80, 17);
            this.chkMalabarista.TabIndex = 15;
            this.chkMalabarista.Text = "Malabarista";
            this.chkMalabarista.UseVisualStyleBackColor = true;
            // 
            // chkMagia
            // 
            this.chkMagia.AutoSize = true;
            this.chkMagia.Location = new System.Drawing.Point(214, 31);
            this.chkMagia.Name = "chkMagia";
            this.chkMagia.Size = new System.Drawing.Size(87, 17);
            this.chkMagia.TabIndex = 14;
            this.chkMagia.Text = "Hacer Magia";
            this.chkMagia.UseVisualStyleBackColor = true;
            // 
            // chkImitar
            // 
            this.chkImitar.AutoSize = true;
            this.chkImitar.Location = new System.Drawing.Point(26, 100);
            this.chkImitar.Name = "chkImitar";
            this.chkImitar.Size = new System.Drawing.Size(51, 17);
            this.chkImitar.TabIndex = 13;
            this.chkImitar.Text = "Imitar";
            this.chkImitar.UseVisualStyleBackColor = true;
            // 
            // chkBailar
            // 
            this.chkBailar.AutoSize = true;
            this.chkBailar.Location = new System.Drawing.Point(26, 77);
            this.chkBailar.Name = "chkBailar";
            this.chkBailar.Size = new System.Drawing.Size(52, 17);
            this.chkBailar.TabIndex = 12;
            this.chkBailar.Text = "Bailar";
            this.chkBailar.UseVisualStyleBackColor = true;
            // 
            // chkPayaso
            // 
            this.chkPayaso.AutoSize = true;
            this.chkPayaso.Location = new System.Drawing.Point(26, 54);
            this.chkPayaso.Name = "chkPayaso";
            this.chkPayaso.Size = new System.Drawing.Size(61, 17);
            this.chkPayaso.TabIndex = 11;
            this.chkPayaso.Text = "Payaso";
            this.chkPayaso.UseVisualStyleBackColor = true;
            // 
            // chkCantar
            // 
            this.chkCantar.AutoSize = true;
            this.chkCantar.Location = new System.Drawing.Point(26, 31);
            this.chkCantar.Name = "chkCantar";
            this.chkCantar.Size = new System.Drawing.Size(57, 17);
            this.chkCantar.TabIndex = 10;
            this.chkCantar.Text = "Cantar";
            this.chkCantar.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1053, 590);
            this.Controls.Add(this.grpSueldo);
            this.Controls.Add(this.grpDatos);
            this.Controls.Add(this.grpExperiencia);
            this.Controls.Add(this.grpHabilidades);
            this.Controls.Add(this.grpDatosgenerales);
            this.Name = "Form1";
            this.Text = "Convocatorio Circo";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.grpDatosgenerales.ResumeLayout(false);
            this.grpDatosgenerales.PerformLayout();
            this.grpSueldo.ResumeLayout(false);
            this.grpSueldo.PerformLayout();
            this.grpDatos.ResumeLayout(false);
            this.grpDatos.PerformLayout();
            this.grpExperiencia.ResumeLayout(false);
            this.grpExperiencia.PerformLayout();
            this.grpHabilidades.ResumeLayout(false);
            this.grpHabilidades.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpDatosgenerales;
        private System.Windows.Forms.RadioButton rbFemenino;
        private System.Windows.Forms.RadioButton rbMasculino;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtDNI;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker dtFecnac;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtAN;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox grpSueldo;
        private System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtUtilidades;
        private System.Windows.Forms.TextBox txtEscolaridad;
        private System.Windows.Forms.LinkLabel linkLabel2;
        private System.Windows.Forms.TextBox txtVida;
        private System.Windows.Forms.TextBox txtESSALUD;
        private System.Windows.Forms.TextBox txtAFP;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtMonto;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.GroupBox grpDatos;
        private System.Windows.Forms.TextBox txtSueldo;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button btnCalcular;
        private System.Windows.Forms.ListBox lstContratar;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btnQuitar;
        private System.Windows.Forms.Button btnAgregar;
        private System.Windows.Forms.ListBox lstPuestos;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cboSede;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox grpExperiencia;
        private System.Windows.Forms.Button btnEvaluar;
        private System.Windows.Forms.TextBox txtActividad;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtSalarial;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.RadioButton rbMasaños;
        private System.Windows.Forms.RadioButton rb4años;
        private System.Windows.Forms.RadioButton rb2años;
        private System.Windows.Forms.RadioButton rbExperiencia;
        private System.Windows.Forms.GroupBox grpHabilidades;
        private System.Windows.Forms.TextBox txtOtros;
        private System.Windows.Forms.CheckBox chkOtros;
        private System.Windows.Forms.CheckBox chkCeremonia;
        private System.Windows.Forms.CheckBox chkMalabarista;
        private System.Windows.Forms.CheckBox chkMagia;
        private System.Windows.Forms.CheckBox chkImitar;
        private System.Windows.Forms.CheckBox chkBailar;
        private System.Windows.Forms.CheckBox chkPayaso;
        private System.Windows.Forms.CheckBox chkCantar;
    }
}

