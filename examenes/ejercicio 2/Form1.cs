﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ejercicio_2
{
    public partial class Form1 : Form
    {
        static int edad = 0;
        static double dinero = 0;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            cboSede.Items.Add("Real Plaza");
            cboSede.Items.Add("Plaza Norte");
            cboSede.Items.Add("Mall del Sur");
            txtActividad.Enabled = false;
            grpDatos.Enabled = false;
            grpSueldo.Enabled = false;
            btnEvaluar.Enabled = false;
            grpExperiencia.Enabled = false;
            grpHabilidades.Enabled = false;
        }
     
        
        

        
       
        

        private void grpDatos_Enter(object sender, EventArgs e)
        {
        }

        private void dtFecnac_ValueChanged_1(object sender, EventArgs e)
        {
            int yearCurrent = DateTime.Now.Year;
            int yearSelect = dtFecnac.Value.Year;
            edad = yearCurrent - yearSelect;
            if (edad >= 18)
            {

                grpHabilidades.Enabled = true;
                grpExperiencia.Enabled = true;
                btnEvaluar.Enabled = true;

            }
            else
            {
                MessageBox.Show("Usted no Cuenta con el Perfil requerido,Intente en otra oportundad", "Lo Sentimos");


            }



        }

        private void rbExperiencia_CheckedChanged_1(object sender, EventArgs e)
        {
            if (rbExperiencia.Checked)
            {
                txtActividad.Enabled = false;
                grpHabilidades.Enabled = true;
            }
        }

        private void rb2años_CheckedChanged_1(object sender, EventArgs e)
        {
            if (rb2años.Checked)
            {
                txtActividad.Enabled = true;
                grpHabilidades.Enabled = true;
            }
        }

        private void rb4años_CheckedChanged_1(object sender, EventArgs e)
        {
            if (rb4años.Checked)
            {
                txtActividad.Enabled = true;
                grpHabilidades.Enabled = true;
            }
        }

        private void rbMasaños_CheckedChanged_1(object sender, EventArgs e)
        {
            if (rbMasaños.Checked)
            {
                grpHabilidades.Enabled = false;
            }
        }

        private void btnEvaluar_Click_1(object sender, EventArgs e)
        {
            grpDatos.Enabled = true;
            grpSueldo.Enabled = true;
        }

        private void cboSede_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            if (cboSede.SelectedIndex == 0)
            {
                lstPuestos.Items.Clear();
                lstPuestos.Items.Add("Payasos");
                lstPuestos.Items.Add("Malabaristas");
                lstPuestos.Items.Add("Cantantes");
                lstPuestos.Items.Add("Bailarines");
            }
            else if (cboSede.SelectedIndex == 1)
            {
                lstPuestos.Items.Clear();
                lstPuestos.Items.Add("Payasos");
                lstPuestos.Items.Add("Malabaristas");
            }
            else
            {
                lstPuestos.Items.Clear();
                lstPuestos.Items.Add("Payasos");
                lstPuestos.Items.Add("Malabaristas");
                lstPuestos.Items.Add("Cantantes");
                lstPuestos.Items.Add("Bailarines");
                lstPuestos.Items.Add("Imitadores");
                lstPuestos.Items.Add("Magos");
                lstPuestos.Items.Add("Maestro de Ceremonia");

            }
        }

        private void btnAgregar_Click_1(object sender, EventArgs e)
        {

            switch (lstPuestos.SelectedItem)
            {
                case "Payasos":
                    dinero = dinero + 1500;
                    break;
                case "Malabaristas":
                    dinero = dinero + 3000;
                    break;
                case "Cantantes":
                    dinero = dinero + 2000;
                    break;
                case "Bailarines":
                    dinero = dinero + 2000;
                    break;
                case "Imitadores":
                    dinero = dinero + 3500;
                    break;
                case "Magos":
                    dinero = dinero + 3000;
                    break;
                case "Maestro de Ceremonia":
                    dinero = dinero + 4000;
                    break;
            }


            switch (lstPuestos.SelectedItem)
            {
                case "Payasos":
                    lstContratar.Items.Add("Payasos-1500");
                    break;
                case "Malabaristas":
                    lstContratar.Items.Add("Malabaristas-3000");
                    break;
                case "Cantantes":
                    lstContratar.Items.Add("Cantantes-2000");
                    break;
                case "Bailarines":
                    lstContratar.Items.Add("Bailarines-2000");
                    break;
                case "Imitadores":
                    lstContratar.Items.Add("Imitadores-3500");
                    break;
                case "Magos":
                    lstContratar.Items.Add("Magos-3000");
                    break;
                case "Maestro de Ceremonia":
                    lstContratar.Items.Add("Maestro de Ceremonia-4000");
                    break;
            }

        }

        private void btnQuitar_Click_1(object sender, EventArgs e)
        {
            double d = 0;

            switch (lstContratar.SelectedItem)
            {
                case "Payasos-1500":
                    d = 1500;
                    break;
                case "Malabaristas-3000":
                    d = 3000;
                    break;
                case "Cantantes-2000":
                    d = 2000;
                    break;
                case "Bailarines-2000":
                    d = 2000;
                    break;
                case "Imitadores-3500":
                    d = 3500;
                    break;
                case "Magos-3500":
                    d = 3000;
                    break;
                case "Maestro de Ceremonia-4000":
                    d = 4000;
                    break;
            }
            lstContratar.Items.Remove(lstContratar.SelectedItem);
            dinero = dinero - d;
        }

        private void lstPuestos_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            switch (lstPuestos.SelectedItem)
            {
                case "Payasos":
                    txtSueldo.Text = "1500";
                    break;
                case "Malabaristas":
                    txtSueldo.Text = "3000";
                    break;
                case "Cantantes":
                    txtSueldo.Text = "2000";
                    break;
                case "Bailarines":
                    txtSueldo.Text = "2000";
                    break;
                case "Imitadores":
                    txtSueldo.Text = "3500";
                    break;
                case "Magos":
                    txtSueldo.Text = "3000";
                    break;
                case "Maestro de Ceremonia":
                    txtSueldo.Text = "4000";
                    break;



            }
        }

        private void btnCalcular_Click_1(object sender, EventArgs e)
        {
            txtMonto.Text = dinero.ToString();
            double afp = dinero * 12 / 100;
            double essalud = dinero * 10 / 100;
            double vida = dinero * 5 / 100;
            double escolar = dinero * 8 / 100;
            double util = dinero * 15 / 100;
            double total = dinero - (afp + essalud + vida) + (escolar + util);
            txtAFP.Text = afp.ToString();
            txtESSALUD.Text = essalud.ToString();
            txtVida.Text = vida.ToString();
            txtUtilidades.Text = util.ToString();
            txtEscolaridad.Text = escolar.ToString();
            lblTotal.Text = $"TOTAL NETO: S/{total}";
        }
    }
}
