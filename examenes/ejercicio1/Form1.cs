﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ejercicio1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

            cboProvincia.Enabled = false;
            cboDistrito.Enabled = false;
            cboDepartamento.Enabled = false;

            grupVotacion.Visible = false;
            btnRegistrar.Enabled = false;
            cboDocumento.Items.Add("DNI");
            cboDocumento.Items.Add("Carnet Ext.");
            cboDepartamento.Items.Add("LIMA");

            cboProvincia.Items.Add("LIMA");
            cboProvincia.Items.Add("HUARAL");
            cboProvincia.Items.Add("HUAROCHIRI");



        }

       

        

        
        private void grupVotacion_Enter(object sender, EventArgs e)
        {
        }

        private void cboDepartamento_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            cboProvincia.Enabled = true;
        }

        private void cboProvincia_SelectedIndexChanged_1(object sender, EventArgs e)
        {

            cboDistrito.Items.Clear();
            cboDistrito.Enabled = true;
            if (cboProvincia.SelectedItem.ToString() == "LIMA")
            {
                cboDistrito.Items.Add("LIMA");
                cboDistrito.Items.Add("ANCON");
                cboDistrito.Items.Add("ATE");
                cboDistrito.Items.Add("BREÑA");
                cboDistrito.Items.Add("CARABAYLLO");
                cboDistrito.Items.Add("COMAS");
                cboDistrito.Items.Add("CHACLACAYO");
                cboDistrito.Items.Add("CHORRILLOS");
                cboDistrito.Items.Add("LA VICTORIA");

            }
            else if (cboProvincia.SelectedItem.ToString() == "HUARAL")
            {
                cboDistrito.Items.Add("HUARAL");
                cboDistrito.Items.Add("ATAVILLOS ALTOS");
                cboDistrito.Items.Add("ATAVILLOS BAJOS");


            }
            else
            {
                cboDistrito.Items.Add("MATUCANA");
                cboDistrito.Items.Add("ANTIOQUIA");
                cboDistrito.Items.Add("CALLAHUANCA");

            }

        }

        private void cboDistrito_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            string n = string.Empty;

            if (cboProvincia.SelectedIndex == 0)
            {

                switch (cboDistrito.SelectedIndex)
                {
                    case 0:
                        n = "140101";
                        break;
                    case 1:
                        n = "140102";
                        break;
                    case 2:
                        n = "140103";
                        break;
                    case 3:
                        n = "140104";
                        break;
                    case 4:
                        n = "140105";
                        break;
                    case 5:
                        n = "140106";
                        break;
                    case 6:
                        n = "140107";
                        break;
                    case 7:
                        n = "140108";
                        break;
                    case 8:
                        n = "140109";
                        break;

                }
            }
            else if (cboProvincia.SelectedIndex == 1)
            {
                switch (cboDistrito.SelectedIndex)
                {
                    case 0:
                        n = "140801";
                        break;
                    case 1:
                        n = "140802";
                        break;
                    case 2:
                        n = "140803";
                        break;
                }
            }
            else
            {
                switch (cboDistrito.SelectedIndex)
                {
                    case 0:
                        n = "140601";
                        break;
                    case 1:
                        n = "140602";
                        break;
                    case 2:
                        n = "140603";
                        break;
                }
            }
            txtUbiego.Text = n;

            


        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            if (txtApellidos.Text.Equals(string.Empty) || txtNombres.Text.Equals(string.Empty))
            {
                MessageBox.Show("Ingrese Apellidos y Nombres", "Atención");
            }
            else
            {
                cboDocumento.Visible = true;
                txtNumero.Visible = true;
                if (txtNumero.Text.Equals(string.Empty) || cboDocumento.SelectedIndex == -1)
                {
                    MessageBox.Show("Introdusca todos los Datos", "Atención");
                }
                else
                {
                    if (cboDocumento.SelectedIndex == 0)
                    {
                        string numero = txtNumero.Text;
                        string apellido = txtApellidos.Text;
                        string nombre = txtNombres.Text;
                        lstLista1.Items.Add($"{nombre} {apellido} -DNI- {numero}");
                    }
                    else
                    {
                        string numero = txtNumero.Text;
                        string apellido = txtApellidos.Text;
                        string nombre = txtNombres.Text;
                        lstLista1.Items.Add($"{nombre} {apellido} -CE- {numero}");

                    }
                }

                int cantidad = lstLista1.Items.Count;
                if (cantidad >= 1)
                {
                    btnRegistrar.Enabled = true;
                }
                lblConteo.Text = "N° de Personas: " + cantidad;
                txtApellidos.Clear();
                txtNombres.Clear();
                txtNumero.Clear();
                txtApellidos.Focus();
            }
        }

        private void rbDescripcion_CheckedChanged_1(object sender, EventArgs e)
        {
            if (rbDescripcion.Checked)
            {
                grupVotacion.Visible = true;
                cboDepartamento.Enabled = true;


            }
        }

        private void rbUbiego_CheckedChanged_1(object sender, EventArgs e)
        {
            if (rbUbiego.Checked)
            {
                grupVotacion.Visible = true;
                cboDepartamento.Visible = false;
                cboDistrito.Visible = false;
                cboProvincia.Visible = false;

            }
        }

        private void btnRegistrar_Click_1(object sender, EventArgs e)
        {
            string c = txtUbiego.Text;
            if (c == String.Empty || txtMesa.Text == string.Empty)
            {
                MessageBox.Show("Faltan Datos", "Alerta");
            }
            else
            {
                if (rbUbiego.Checked)
                {
                    switch (c)
                    {
                        case "140101":
                            lstLista2.Items.Add($"140101-LIMA");
                            break;
                        case "140102":
                            lstLista2.Items.Add($"140102-LIMA");
                            break;
                        case "140103":
                            lstLista2.Items.Add($"140103-LIMA");
                            break;
                        case "140104":
                            lstLista2.Items.Add($"140104-LIMA");
                            break;
                        case "140105":
                            lstLista2.Items.Add($"140105-LIMA");
                            break;
                        case "140106":
                            lstLista2.Items.Add($"140106-LIMA");
                            break;
                        case "140107":
                            lstLista2.Items.Add($"140107-LIMA");
                            break;
                        case "140108":
                            lstLista2.Items.Add($"140108-LIMA");
                            break;
                        case "140109":
                            lstLista2.Items.Add($"140109-LIMA");
                            break;
                        case "140801":
                            lstLista2.Items.Add($"140801-HUARAL");
                            break;
                        case "140802":
                            lstLista2.Items.Add($"140802-HURAL");
                            break;
                        case "140803":
                            lstLista2.Items.Add($"140803-HUARAL");
                            break;
                        case "140601":
                            lstLista2.Items.Add($"140601-HUAROCHIRI");
                            break;
                        case "140602":
                            lstLista2.Items.Add($"140602-HUAROCHIRI");
                            break;
                        case "140603":
                            lstLista2.Items.Add($"140603-HUAROCHIRI");
                            break;
                        default:
                            MessageBox.Show("Ubigeo Erroneo", "Atención");
                            break;

                    }
                    MessageBox.Show("Sorteo Realizado Exitosamente", "Atención");
                    txtUbiego.Clear();
                    txtMesa.Clear();
                    txtUbiego.Focus();
                }
                else
                {
                    txtUbiego.Clear();
                    txtMesa.Clear();
                    txtUbiego.Focus();
                    lstLista2.Items.Add($"{c}-{cboProvincia.SelectedItem}");

                }

            }

        }
    }
    }

