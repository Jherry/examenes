﻿namespace ejercicio1
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.grupVotacion = new System.Windows.Forms.GroupBox();
            this.btnRegistrar = new System.Windows.Forms.Button();
            this.txtMesa = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtUbiego = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.rbSuplente = new System.Windows.Forms.RadioButton();
            this.rbSecretario = new System.Windows.Forms.RadioButton();
            this.rbPresidente = new System.Windows.Forms.RadioButton();
            this.label12 = new System.Windows.Forms.Label();
            this.cboDistrito = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.cboProvincia = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.cboDepartamento = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.lstLista3 = new System.Windows.Forms.ListBox();
            this.label8 = new System.Windows.Forms.Label();
            this.lstLista2 = new System.Windows.Forms.ListBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lstLista1 = new System.Windows.Forms.ListBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cboDocumento = new System.Windows.Forms.ComboBox();
            this.lblConteo = new System.Windows.Forms.Label();
            this.btnAgregar = new System.Windows.Forms.Button();
            this.txtNumero = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtNombres = new System.Windows.Forms.TextBox();
            this.txtApellidos = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.rbDescripcion = new System.Windows.Forms.RadioButton();
            this.rbUbiego = new System.Windows.Forms.RadioButton();
            this.grupVotacion.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // grupVotacion
            // 
            this.grupVotacion.Controls.Add(this.btnRegistrar);
            this.grupVotacion.Controls.Add(this.txtMesa);
            this.grupVotacion.Controls.Add(this.label14);
            this.grupVotacion.Controls.Add(this.txtUbiego);
            this.grupVotacion.Controls.Add(this.label13);
            this.grupVotacion.Controls.Add(this.rbSuplente);
            this.grupVotacion.Controls.Add(this.rbSecretario);
            this.grupVotacion.Controls.Add(this.rbPresidente);
            this.grupVotacion.Controls.Add(this.label12);
            this.grupVotacion.Controls.Add(this.cboDistrito);
            this.grupVotacion.Controls.Add(this.label11);
            this.grupVotacion.Controls.Add(this.cboProvincia);
            this.grupVotacion.Controls.Add(this.label10);
            this.grupVotacion.Controls.Add(this.cboDepartamento);
            this.grupVotacion.Controls.Add(this.label9);
            this.grupVotacion.Location = new System.Drawing.Point(299, 76);
            this.grupVotacion.Name = "grupVotacion";
            this.grupVotacion.Size = new System.Drawing.Size(486, 173);
            this.grupVotacion.TabIndex = 41;
            this.grupVotacion.TabStop = false;
            this.grupVotacion.Text = "Area de Votación";
            // 
            // btnRegistrar
            // 
            this.btnRegistrar.Location = new System.Drawing.Point(241, 128);
            this.btnRegistrar.Name = "btnRegistrar";
            this.btnRegistrar.Size = new System.Drawing.Size(121, 23);
            this.btnRegistrar.TabIndex = 33;
            this.btnRegistrar.Text = "Registrar Sorteo";
            this.btnRegistrar.UseVisualStyleBackColor = true;
            this.btnRegistrar.Click += new System.EventHandler(this.btnRegistrar_Click_1);
            // 
            // txtMesa
            // 
            this.txtMesa.Location = new System.Drawing.Point(373, 65);
            this.txtMesa.Name = "txtMesa";
            this.txtMesa.Size = new System.Drawing.Size(96, 20);
            this.txtMesa.TabIndex = 32;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(273, 68);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(95, 13);
            this.label14.TabIndex = 31;
            this.label14.Text = "Mesa de votacion:";
            // 
            // txtUbiego
            // 
            this.txtUbiego.Location = new System.Drawing.Point(373, 22);
            this.txtUbiego.Name = "txtUbiego";
            this.txtUbiego.Size = new System.Drawing.Size(96, 20);
            this.txtUbiego.TabIndex = 30;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(273, 25);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(44, 13);
            this.label13.TabIndex = 29;
            this.label13.Text = "Ubigeo:";
            // 
            // rbSuplente
            // 
            this.rbSuplente.AutoSize = true;
            this.rbSuplente.Location = new System.Drawing.Point(180, 91);
            this.rbSuplente.Name = "rbSuplente";
            this.rbSuplente.Size = new System.Drawing.Size(67, 17);
            this.rbSuplente.TabIndex = 28;
            this.rbSuplente.Text = "Suplente";
            this.rbSuplente.UseVisualStyleBackColor = true;
            // 
            // rbSecretario
            // 
            this.rbSecretario.AutoSize = true;
            this.rbSecretario.Location = new System.Drawing.Point(180, 68);
            this.rbSecretario.Name = "rbSecretario";
            this.rbSecretario.Size = new System.Drawing.Size(73, 17);
            this.rbSecretario.TabIndex = 27;
            this.rbSecretario.Text = "Secretario";
            this.rbSecretario.UseVisualStyleBackColor = true;
            // 
            // rbPresidente
            // 
            this.rbPresidente.AutoSize = true;
            this.rbPresidente.Checked = true;
            this.rbPresidente.Location = new System.Drawing.Point(180, 45);
            this.rbPresidente.Name = "rbPresidente";
            this.rbPresidente.Size = new System.Drawing.Size(75, 17);
            this.rbPresidente.TabIndex = 26;
            this.rbPresidente.TabStop = true;
            this.rbPresidente.Text = "Presidente";
            this.rbPresidente.UseVisualStyleBackColor = true;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(177, 25);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(38, 13);
            this.label12.TabIndex = 25;
            this.label12.Text = "Cargo:";
            // 
            // cboDistrito
            // 
            this.cboDistrito.FormattingEnabled = true;
            this.cboDistrito.Location = new System.Drawing.Point(7, 139);
            this.cboDistrito.Name = "cboDistrito";
            this.cboDistrito.Size = new System.Drawing.Size(147, 21);
            this.cboDistrito.TabIndex = 24;
            this.cboDistrito.SelectedIndexChanged += new System.EventHandler(this.cboDistrito_SelectedIndexChanged_1);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(4, 123);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(42, 13);
            this.label11.TabIndex = 23;
            this.label11.Text = "Distrito:";
            // 
            // cboProvincia
            // 
            this.cboProvincia.FormattingEnabled = true;
            this.cboProvincia.Location = new System.Drawing.Point(7, 91);
            this.cboProvincia.Name = "cboProvincia";
            this.cboProvincia.Size = new System.Drawing.Size(147, 21);
            this.cboProvincia.TabIndex = 22;
            this.cboProvincia.SelectedIndexChanged += new System.EventHandler(this.cboProvincia_SelectedIndexChanged_1);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(5, 75);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(54, 13);
            this.label10.TabIndex = 21;
            this.label10.Text = "Provincia:";
            // 
            // cboDepartamento
            // 
            this.cboDepartamento.FormattingEnabled = true;
            this.cboDepartamento.Location = new System.Drawing.Point(7, 41);
            this.cboDepartamento.Name = "cboDepartamento";
            this.cboDepartamento.Size = new System.Drawing.Size(147, 21);
            this.cboDepartamento.TabIndex = 20;
            this.cboDepartamento.SelectedIndexChanged += new System.EventHandler(this.cboDepartamento_SelectedIndexChanged_1);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(5, 25);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(77, 13);
            this.label9.TabIndex = 19;
            this.label9.Text = "Departamento:";
            // 
            // lstLista3
            // 
            this.lstLista3.FormattingEnabled = true;
            this.lstLista3.Location = new System.Drawing.Point(568, 286);
            this.lstLista3.Name = "lstLista3";
            this.lstLista3.Size = new System.Drawing.Size(217, 134);
            this.lstLista3.TabIndex = 40;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(565, 258);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(96, 13);
            this.label8.TabIndex = 39;
            this.label8.Text = "Miembros de Mesa";
            // 
            // lstLista2
            // 
            this.lstLista2.FormattingEnabled = true;
            this.lstLista2.Location = new System.Drawing.Point(309, 286);
            this.lstLista2.Name = "lstLista2";
            this.lstLista2.Size = new System.Drawing.Size(217, 134);
            this.lstLista2.TabIndex = 38;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(306, 260);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(97, 13);
            this.label7.TabIndex = 37;
            this.label7.Text = "Ubigeos Sorteados";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(15, 260);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(93, 13);
            this.label6.TabIndex = 36;
            this.label6.Text = "Personas Inscritas";
            // 
            // lstLista1
            // 
            this.lstLista1.FormattingEnabled = true;
            this.lstLista1.Location = new System.Drawing.Point(18, 286);
            this.lstLista1.Name = "lstLista1";
            this.lstLista1.Size = new System.Drawing.Size(268, 134);
            this.lstLista1.TabIndex = 35;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cboDocumento);
            this.groupBox1.Controls.Add(this.lblConteo);
            this.groupBox1.Controls.Add(this.btnAgregar);
            this.groupBox1.Controls.Add(this.txtNumero);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtNombres);
            this.groupBox1.Controls.Add(this.txtApellidos);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(15, 30);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(271, 197);
            this.groupBox1.TabIndex = 34;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Datos Generales:";
            // 
            // cboDocumento
            // 
            this.cboDocumento.FormattingEnabled = true;
            this.cboDocumento.Location = new System.Drawing.Point(19, 100);
            this.cboDocumento.Name = "cboDocumento";
            this.cboDocumento.Size = new System.Drawing.Size(121, 21);
            this.cboDocumento.TabIndex = 11;
            // 
            // lblConteo
            // 
            this.lblConteo.AutoSize = true;
            this.lblConteo.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblConteo.Location = new System.Drawing.Point(94, 155);
            this.lblConteo.Name = "lblConteo";
            this.lblConteo.Size = new System.Drawing.Size(0, 24);
            this.lblConteo.TabIndex = 9;
            // 
            // btnAgregar
            // 
            this.btnAgregar.Location = new System.Drawing.Point(17, 146);
            this.btnAgregar.Name = "btnAgregar";
            this.btnAgregar.Size = new System.Drawing.Size(76, 33);
            this.btnAgregar.TabIndex = 8;
            this.btnAgregar.Text = "Agregar";
            this.btnAgregar.UseVisualStyleBackColor = true;
            this.btnAgregar.Click += new System.EventHandler(this.btnAgregar_Click);
            // 
            // txtNumero
            // 
            this.txtNumero.Location = new System.Drawing.Point(156, 102);
            this.txtNumero.Name = "txtNumero";
            this.txtNumero.Size = new System.Drawing.Size(94, 20);
            this.txtNumero.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(153, 82);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Numero";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 82);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(26, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "DNI";
            // 
            // txtNombres
            // 
            this.txtNombres.Location = new System.Drawing.Point(71, 50);
            this.txtNombres.Name = "txtNombres";
            this.txtNombres.Size = new System.Drawing.Size(180, 20);
            this.txtNombres.TabIndex = 3;
            // 
            // txtApellidos
            // 
            this.txtApellidos.Location = new System.Drawing.Point(71, 19);
            this.txtApellidos.Name = "txtApellidos";
            this.txtApellidos.Size = new System.Drawing.Size(180, 20);
            this.txtApellidos.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Nombres:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Apellidos:";
            // 
            // rbDescripcion
            // 
            this.rbDescripcion.AutoSize = true;
            this.rbDescripcion.Location = new System.Drawing.Point(399, 35);
            this.rbDescripcion.Name = "rbDescripcion";
            this.rbDescripcion.Size = new System.Drawing.Size(98, 17);
            this.rbDescripcion.TabIndex = 43;
            this.rbDescripcion.Text = "DESCRIPCIÓN";
            this.rbDescripcion.UseVisualStyleBackColor = true;
            this.rbDescripcion.CheckedChanged += new System.EventHandler(this.rbDescripcion_CheckedChanged_1);
            // 
            // rbUbiego
            // 
            this.rbUbiego.AutoSize = true;
            this.rbUbiego.Location = new System.Drawing.Point(578, 35);
            this.rbUbiego.Name = "rbUbiego";
            this.rbUbiego.Size = new System.Drawing.Size(66, 17);
            this.rbUbiego.TabIndex = 42;
            this.rbUbiego.Text = "UBIGEO";
            this.rbUbiego.UseVisualStyleBackColor = true;
            this.rbUbiego.CheckedChanged += new System.EventHandler(this.rbUbiego_CheckedChanged_1);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.rbDescripcion);
            this.Controls.Add(this.rbUbiego);
            this.Controls.Add(this.grupVotacion);
            this.Controls.Add(this.lstLista3);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.lstLista2);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.lstLista1);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.grupVotacion.ResumeLayout(false);
            this.grupVotacion.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox grupVotacion;
        private System.Windows.Forms.Button btnRegistrar;
        private System.Windows.Forms.TextBox txtMesa;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtUbiego;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.RadioButton rbSuplente;
        private System.Windows.Forms.RadioButton rbSecretario;
        private System.Windows.Forms.RadioButton rbPresidente;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox cboDistrito;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cboProvincia;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cboDepartamento;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ListBox lstLista3;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ListBox lstLista2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ListBox lstLista1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox cboDocumento;
        private System.Windows.Forms.Label lblConteo;
        private System.Windows.Forms.Button btnAgregar;
        private System.Windows.Forms.TextBox txtNumero;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtNombres;
        private System.Windows.Forms.TextBox txtApellidos;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton rbDescripcion;
        private System.Windows.Forms.RadioButton rbUbiego;
    }
}

